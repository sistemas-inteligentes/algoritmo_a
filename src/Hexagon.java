import java.util.Scanner;

public class Hexagon implements Comparable<Hexagon>{

	//ATRIBUTOS
	
	private int matriz[][];
	private int NUMERO_DE_PIEZAS = 7;
	private int NUMERO_DE_LADOS = 6;
	int PiezasColocadas = 0;
	int Costo = 0;
	int Nivel = 0;
	int Costo_F;
	//METODOS
	Hexagon(){
		matriz = new int[NUMERO_DE_PIEZAS][NUMERO_DE_LADOS];
	}
	
	public int Calcular_F()
	{
		return Costo_F = Costo + Calcular_h();
	}
	
	public void LeerDatos() 
	{
		Scanner stdin = new Scanner(System.in);
		for (int i=0; i<NUMERO_DE_PIEZAS; i++)
			for (int j=0; j<NUMERO_DE_LADOS; j++)
				this.matriz[i][j] = stdin.nextInt();
	}
	
	public int Numero_Piezas(int valor_h)
	{
		if (matriz[0][0]==1 && valor_h==0) {
			return 1;
		}
		switch (valor_h) {
		case -1:
			return 2;
		case -3:
			return 3;
		case -5:
			return 4;
		case -7:
			return 5;
		case -9:
			return 6;
		case -11:
			return 7;
		case -12:
			return 7;
		}
		return 0;
	}
	
	//PARA CALCULAR TAMANIO
	public int ObtenerNumeroDePiezas() {
		return PiezasColocadas;
	}
	
	public void Mostrar()
	{
		for (int x=0; x < NUMERO_DE_PIEZAS; x++) {
			  System.out.print("|");
			  for (int y=0; y <NUMERO_DE_LADOS; y++) {
				System.out.print(matriz[x][y]);
				if (y!=matriz[x].length-1) System.out.print("\t");
			  }
			  System.out.println("|");
			}
	    System.out.println("COSTO :: " + Costo); 
	    System.out.println("HEURISTICA :: " + Calcular_h()); 
	    System.out.println("COSTO H :: " + Calcular_F());
	}
	
	//PARA SETEAR NEVO ESTADO
	public int[][] ObtenerMatriz(){
		return this.matriz;
	}
	
	public void CopiarMatriz(int x[][]) {
		for (int i=0; i<NUMERO_DE_PIEZAS; i++)
			for (int j=0; j<NUMERO_DE_LADOS; j++)
				this.matriz[i][j] = x[i][j];
	}
	
	//COLOCAR CENTRO
	public void Iniciar() {
		Rotar(matriz[0], 0, 1);
	}
	
	public  void AplicarRegla(int valor,int HexaDest)
	{	
		if (valor >= 0 && HexaDest <=6 && valor <= 6 && HexaDest >=0)
		{
			if (valor!=HexaDest)
			{
				SacarYColocarHexagono(valor, HexaDest);
					
			}
			if (valor==0) {
				PiezasColocadas=1;
				Iniciar();
			}
			else 
			{
				if (valor<=NUMERO_DE_LADOS)
				{
					Rotar(matriz[valor], (valor+2)%NUMERO_DE_LADOS, matriz[0][valor-1]);		
				}	
			}
		}
		Nivel++;
	}
	//CONTROL DE LA REGLA APLICABLE
	public boolean ControlarReglasAplicables(int valor_h) 
	{
		switch (Nivel) {
		case 1:
			if (valor_h<= 0) 
				return true;
		case 2:
			if (valor_h<=-1) 
				return true;
		case 3:
			if (valor_h<=-3) 
				return true;
		case 4:
			if (valor_h<=-5) 
				return true;
		case 5:
			if (valor_h<=-7) 
			return true;
		case 6:
			if (valor_h<=-9) 
				return true;
		case 7:
			if (valor_h<=-11 || valor_h<=-12) 
				return true;
		}
		return false;
	}
	//REGLAS DE IMPLEMENTACION
	//SACAR Y COLOCAR HEXAGONO
	public void SacarYColocarHexagono( int HexaOri,int HexaDest) {
		 Costo = Costo+2;
		int Vec1[] = matriz[HexaOri];
		matriz[HexaOri] =  matriz[HexaDest];
		matriz[HexaDest] = Vec1;
	}
	//ROTACIONES
	private void Rotar(int[] pieza, int Posicion, int number) {
		
		while (pieza[Posicion] != number) {
		    Costo++;
			Girar(pieza);
		}
	}
	private void Girar(int[] pieza) {
		int auxiliar = pieza[0];
		for (int i=1; i<NUMERO_DE_LADOS; i++) {
			pieza[i-1] = pieza[i];
		}
		pieza[NUMERO_DE_LADOS-1] = auxiliar;
	}
	//NOS AYUDARA A BUSCAR ESTADOS IGUALES EN LA LISTA TABU
	public boolean SonIguales(Hexagon EstadoActual) {
		int [][]matrizHexagonoActual = EstadoActual.ObtenerMatriz();
		for (int x=0; x < NUMERO_DE_PIEZAS; x++) 
			for (int y=0; y <NUMERO_DE_LADOS; y++) 
				if (matriz[x][y] != matrizHexagonoActual[x][y] ) 
					return false;
		return true;
	}

	
	public boolean TienenElMismoVector(Hexagon H2, int size)
	{
		int [][]matrizHexagonoActual = H2.ObtenerMatriz();
		for (int x=0; x < size; x++) 
			for (int y=0; y <NUMERO_DE_LADOS; y++) 
				if (matriz[x][y] != matrizHexagonoActual[x][y] ) 
					return false;
		return true;
	}
	
	//HEURISTICA
	//NI LA NASA TENIA UNA HEURISTICA TAN FEA PERO ALMENOS FUNCIONA V,:
	
	//ESTE CALCULAR_H REVISA TODO EL PROBLEMA PARA VERIFICAR SI LOS LADOS COINCIDEN
	// H = 1 , SOLO CENTRO COINCIDE
	// H = 3 , CENTRO Y HEXA SUPERIOR COINCIDEN
	// H = 5 , CENTRO, HEXA SUP Y HEXA SUP DER CONCIDEN
	// Y ASI... HASTA H = 12, TODOS LOS LADOS COINCIDEN Y ES LA SOLUCION XD :3
	// PODRIAMOS DECIR QUE NUESTRA CONDICION DE TERMINACION ES 12 :o ES ALGO QUE NO PROBE AUN 
	// PERO AYUDA BASTANTE AL ALGORITMO 	
	public int Calcular_h() {
		//SETEAR VARIABLES QUE AYUDAN A CONTROLAR QUE EL HEXAGONO NO SE SALGA DE LOS BORDES V,,:
		int j=3;
		int k=1;
		int l=0;
		int valor_h =0;
		for (int i = 0; i < NUMERO_DE_PIEZAS; i++) 
		{
			//CONTROLAR QUE NO SE SALGA...
			if (j==6)
			{
				j=0;
			}
			if (k==7) {
				k=0;
			}
			int valor = i;
			int siguiente = valor + 1;
			if (siguiente >= NUMERO_DE_PIEZAS) 
				siguiente = 1;
			//CONTROLAR SOLO EL CENTRO
			if (i==0) 
			{
				//CONTROLA EL CENTRO V:< PERO SOLO LA PRIMERA VEZ PARA SI TIENE UN COMPA ARRIBA
				//SUMA UNO SI ES QUE EL COMPA DE ARRIBA ESTA CHOCANDO BIEN CON EL 1 SUPERIOR DEL CENTRO
				if (matriz[0][l] == matriz[k][j]) 
				{
					valor_h--;
				}
				else
				{
					return valor_h;
				}
			}
			else
			{
				//ESTE IF CONTROLA EL ULTIMO HEXAGONO COLOCADO SI TODO FUE BIEN HASTA AHORA
				if (i == 6)
				{
					if (matriz[i][(i+1)%NUMERO_DE_LADOS] == matriz[siguiente][(i+4)%NUMERO_DE_LADOS]) 
					{
						valor_h--;
					}
				}
				else {
					//ESTE BATO MEDIO FEO CONTROLA TANTO CENTRO COMO EL HEXAGONO ANTERIOR QUE 
					//DEBERIA COLISIONAR.. ME REFIERO A LOS EXTREMOS QUE CHOCAN Y NO SON EL CENTRO..
					//ESTE CHAMACO CONTROLA AMBOS AL VEZ.. SI ES ASI SE SUMA DIRECTO 2 A LA HEURISTICA 
					//POR QUE SE CONFIRMA QUE HAY 2 LADOS IGUALES 
					if ((matriz[0][l] == matriz[k][j]) && (matriz[i][(i+1)%NUMERO_DE_LADOS] == matriz[siguiente][(i+4)%NUMERO_DE_LADOS]))
					{
						valor_h = valor_h-2;
					}
					else
					{
						//SE SALE SI DE LA FUNCION SI NO HAY COINCIDENCIAS...
						return valor_h;
					}	
				}	
			}
			
			//SEGUIR CONTROLANDO QUE NO SE SALGA...........
			if (i<NUMERO_DE_LADOS-1)
			{
				l++;
			}
			j++;
			k++;	
		}
		return valor_h;
	}

	@Override
	public int compareTo(Hexagon o) {
        if(o.Calcular_F()>Calcular_F()){
            return -1;
        }else if(o.Calcular_F()>Calcular_F()){
            return 0;
        }else{
            return 1;
        }
	}
	
}
