
import java.util.ArrayList;

public class AlgoritmoA {

		
		ArrayList<Hexagon> CERRADOS = new ArrayList<Hexagon>();
		ArrayList<Hexagon> ABIERTOS = new ArrayList<Hexagon>();
		
		//Espacios Adyacentes
		ArrayList<Hexagon> EspaciosDeEstadosAdyacentes = new ArrayList<Hexagon>();
		//REGLAS APLICABLES
		ArrayList<Hexagon> ReglasAplicables = new ArrayList<Hexagon>();	
	
		ArrayList<Hexagon> Camino = new ArrayList<Hexagon>();
		
		Hexagon Est_Nuevo = new Hexagon();
		Hexagon Est_Actual = new Hexagon();
	
		Hexagon Estado_Final = new Hexagon();
		Hexagon Estado_Inicial = new Hexagon();
			
		
		int Tam = 0;
		int Num=1;
		boolean exito;
		public long inicio = 0 ;
	    public long fin = 0;
		public int EE_Repetidos=0;
		int Costo_Temp;
		int Registro,E_repetido = 0;
		
		int NumeroDeAbiertos;
		int NumeroDeCerrados;
		
		AlgoritmoA(){
			}
		
		public void Construir_Camino() {
			
			Copiar_Estado(Estado_Final, Est_Actual);
			
			System.out.println(" --------------- ESTADO INICIAL --------------");
			Estado_Inicial.Mostrar();			
			System.out.println(" ---------------- ESTADO FINAL ---------------");
			Estado_Final.Mostrar();
			System.out.println(" ---------------- CAMINO ---------------");
			for (int i = 0; i < 7; i++) {
				for (int j = Estado_Inicial.Nivel; j <7; j++) {
					Hexagon Aux = new Hexagon();
					Copiar_Estado(Aux, Estado_Inicial);
					Aux.AplicarRegla(Aux.Nivel, j);
					if (Estado_Final.TienenElMismoVector(Aux, Aux.Nivel)) {
						Aux.Mostrar();		
						Camino.add(Aux);
						Copiar_Estado(Estado_Inicial, Aux);
					}
				}
			}
		}
		
		public void leerDatos() {
			Est_Actual.LeerDatos();
			Copiar_Estado(Estado_Inicial, Est_Actual);
		}
		
		public void Mostrar(Hexagon Estado_Inicial) {
			Estado_Inicial.Mostrar();
		}
		
		private boolean Condicion_term (Hexagon c)
		{
			if (c.Calcular_h() == -12)
			{
				return true;
			}
			return false;
		}
		
		private void LimpiarAplicables()
		{
			ReglasAplicables.clear();
		}
		
		private boolean Vacio(ArrayList<Hexagon> List)
		{
			if (List.size()<=0)
			{
				return true;
			}
			return false;
		}
		
		private Hexagon Sacar_EstadoMF(ArrayList<Hexagon> list)
		{
			Hexagon result = new Hexagon();
			Copiar_Estado(result, list.get(0));
			list.remove(0);
			return result;
		}
		
		public boolean AlgoritmoA() {
			inicio = System.currentTimeMillis();
			exito = false;
			Insertar(Est_Actual,ABIERTOS);
			NumeroDeAbiertos++;
		    System.gc();
		    Runtime rt = Runtime.getRuntime();
		    long usedMB = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;

			while (!Vacio(ABIERTOS) && (exito!=true))
			{
				Est_Actual = Sacar_EstadoMF(ABIERTOS);
				if (Registro!=0)
					NumeroDeCerrados++;
					Insertar(Est_Actual,CERRADOS);	
				if(Condicion_term(Est_Actual))
				{
					System.out.println ("memory usage" + usedMB);
					exito=true;
				}
				else
				{
					
					Tam = ReglasAplicables(Est_Actual);
					if (Tam!=0 && Vacio(ABIERTOS)) // CONTROLAR QUE AUN HAY ESTADOS EN LOS ABIERTOS
					{
						for (int i = 0; i < Tam; i++) 
						{
							Copiar_Estado(Est_Nuevo,ReglasAplicables.get(i));
							Costo_Temp = Est_Nuevo.Calcular_F();
							if (!Buscar(Est_Nuevo, ABIERTOS) && !Buscar(Est_Nuevo, CERRADOS))
							{	 
								Costo_Temp = Est_Nuevo.Costo;
								NumeroDeAbiertos++;
								Insertar(Est_Nuevo,ABIERTOS);
							}
							else
							{
								
								if (Costo_Temp < Est_Actual.Calcular_F())
								{
									Est_Nuevo.Costo_F = Est_Nuevo.Calcular_F();
									if (Buscar(Est_Nuevo, ABIERTOS))
									{
										Actualizar(Est_Nuevo, ABIERTOS);
									}
									if (Buscar(Est_Nuevo, CERRADOS))
									{
										E_repetido++;
										Sacar(Est_Nuevo, CERRADOS);
										NumeroDeAbiertos++;
										Insertar(Est_Nuevo, ABIERTOS);
									}
								}
							}
						}
						LimpiarAplicables();			
					}
				}
				Registro++;
			}
			return exito;
		}
		
		
		
		public void MostrarDatosDeEvaluacion() {
			fin = System.currentTimeMillis();
			long tiempo = (long) ((fin - inicio));
		    System.out.println((tiempo) +" milisegundos");
			System.out.println("Memoria usada :: " + ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory())/(1000*1000) + "mb") );
			System.out.println("TAMA�O ACTUAL DE ABIERTOS :: " +  ABIERTOS.size());
			System.out.println("TAMA�O ACTUAL DE CERRADOS :: " +  CERRADOS.size());
		    System.out.println("ESTADOS REPETIDOS :: " +  E_repetido);
		    System.out.println("TAMA�O MAXIMO DE ABIERTOS :: " +  NumeroDeAbiertos);
			System.out.println("TAMA�O MAXIMO DE CERRADOS :: " +  NumeroDeCerrados);
		}


		private void MostrarReglas() {
			for (int i = 0; i < ReglasAplicables.size(); i++) {
				System.out.println("HEXAGONO :: " + (i+1));
				System.out.println("PIEZAS :: " + ReglasAplicables.get(i).PiezasColocadas);
				System.out.println("VALOR H :: " + ReglasAplicables.get(i).Calcular_h());
				System.out.println("NIVEL :: " + ReglasAplicables.get(i).Nivel);
				System.out.println("NIVEL :: " + ReglasAplicables.get(i).Nivel);
				ReglasAplicables.get(i).Mostrar();
				}
		}
		
		private void Copiar_Estado(Hexagon Est_mejor,Hexagon Est_Actual) {
			Est_mejor.CopiarMatriz(Est_Actual.ObtenerMatriz());
			Est_mejor.Costo=Est_Actual.Costo;
			Est_mejor.PiezasColocadas=Est_Actual.Numero_Piezas(Est_Actual.Calcular_h());
			Est_mejor.Nivel = Est_Actual.Nivel;
		}


		private int ReglasAplicables(Hexagon Est_Actual) {
			int Val=0;		
			for (int j = Est_Actual.Nivel; j < 7; j++) {
				Hexagon Aux = new Hexagon();
				EspaciosDeEstadosAdyacentes.add(Aux);
				Copiar_Estado(EspaciosDeEstadosAdyacentes.get(Val), Est_Actual);
				EspaciosDeEstadosAdyacentes.get(Val).AplicarRegla(EspaciosDeEstadosAdyacentes.get(Val).Nivel, j);
				Copiar_Estado(Aux, EspaciosDeEstadosAdyacentes.get(Val));
				ReglasAplicables.add(Aux);
				Val++;
			}
			return ReglasAplicables.size();
		}
		
		
		// SOLO PARA VERIFICAR SI INSERTA BIEN
		
		public void Insertar(Hexagon Estado_Visitado,ArrayList<Hexagon> Lista) {
			Hexagon Aux = new Hexagon();
			Copiar_Estado(Aux, Estado_Visitado);
			Lista.add(Aux);	
			//Collections.sort(Lista);
		}
		// SOLO PRUEBAS
		
		public void MostrarLista(ArrayList<Hexagon> Lista) {
			for (int i = 0; i <Lista.size(); i++) {
				Lista.get(i).Mostrar();
			}
		}
		
		public boolean Sacar(Hexagon Estado_Visitado,ArrayList<Hexagon> Lista) {
			for (int i = 0; i < Lista.size(); i++) {
				if (Lista.get(i).SonIguales(Estado_Visitado))
					Lista.remove(i);
			}
			return false;
		}
	
		public boolean Actualizar(Hexagon Estado_Visitado,ArrayList<Hexagon> Lista) {
			for (int i = 0; i < Lista.size(); i++) {
				if (Lista.get(i).SonIguales(Estado_Visitado))
					Lista.get(i).Costo_F = Estado_Visitado.Calcular_F();
			}
			return false;
		}
	
		public boolean Buscar(Hexagon Estado_Visitado,ArrayList<Hexagon> Lista) {
			for (int i = 0; i < Lista.size(); i++) {
			
				if (Lista.get(i).SonIguales(Estado_Visitado))
					return true;
			}
			return false;
		}
}
